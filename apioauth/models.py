from email.policy import default
import uuid
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser

class UserManager(BaseUserManager):
     def create_user(self, username, password=None):
          if not username:
               raise ValueError('User must has an username')
          user = self.model(
               username=username,
          )
          user.is_active = True
          user.set_password(password)
          user.save(using=self._db)
          return user
     
     def create_superuser(self, username, password=None):
          if not username:
               raise ValueError("User must have a username!")

          user = self.create_user(
               username=username, 
               password=password,
          )
          user.is_admin = True
          user.is_staff = True
          user.is_superuser = True
          user.save(using=self._db)
          
          return user


class User(AbstractBaseUser):
     username = models.CharField(max_length=500)
     client_id = models.CharField(max_length=500)
     client_secret = models.CharField(max_length=500)
     date_joined = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
     last_login = models.DateTimeField(verbose_name='last login', null=True, auto_now=True)
     is_admin = models.BooleanField(default=False)
     is_active = models.BooleanField(default=True)
     is_staff = models.BooleanField(default=False)
     is_superuser = models.BooleanField(default=False)

     USERNAME_FIELD = 'username'
     objects = UserManager()

     def has_perm(self, perm, obj=None):
          return self.is_admin

     def has_module_perms(self, app_label):
          return True

     def __str__(self) -> str:
         return "Username: {} || Client id: {}".format(self.username, self.client_id)
     
     class Meta:
          unique_together = (('client_id', 'username'),)

class UserProfile(models.Model):
     id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
     user = models.OneToOneField(User, on_delete=models.CASCADE)
     full_name = models.CharField(max_length=500)
     npm = models.CharField(max_length=10)

     def __str__(self) -> str:
         return "{} || Full name: {} || NPM: {}".format(self.user, self.full_name, self.npm)

class TokenAuth(models.Model):
     user = models.ForeignKey(User, on_delete=models.CASCADE)
     access_token = models.CharField(max_length=40, unique=True)
     refresh_token = models.CharField(max_length=40, unique=True)
     created_at = models.DateTimeField(auto_now_add=True, blank=True)

     class Meta:
          unique_together = (('access_token', 'refresh_token'),)