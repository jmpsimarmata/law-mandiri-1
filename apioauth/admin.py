from django.contrib import admin
from .models import User, UserProfile, TokenAuth

class TokenAdmin(admin.ModelAdmin):
     list_display = (
          'user',
          'access_token',
          'refresh_token',
          'created_at'
     )

admin.site.register(User)
admin.site.register(UserProfile)
admin.site.register(TokenAuth, TokenAdmin)