from rest_framework import serializers

from apioauth.models import User
from .token import generate_token

class ObtainTokenSerializer(serializers.Serializer):
     username = serializers.CharField(max_length=500)
     password = serializers.CharField()
     grant_type = serializers.CharField()
     client_id = serializers.CharField(max_length=500)
     client_secret = serializers.CharField(max_length=500)

     def validate_grant_type(self, value):
          if value != 'password':
               raise serializers.ValidationError('Grant type must be password!')
          return value

     def validate(self, data):
          username = data.get('username')
          password = data.get('password')
          client_id = data.get('client_id')
          client_secret = data.get('client_secret')

          user = User.objects.filter(
               username = username,
               password = password,
               client_id = client_id,
               client_secret = client_secret
          )
          if not user:
               raise serializers.ValidationError({
                  'user' : 'You enter the wrong username or password or client id or client secret' 
               })
          
          #sampai blok ini menandakan kalau user nya sudah terautentikasi dengan semua fields yang dimasukkan
          token = generate_token(username, client_id)

          access_token = token.access_token
          expires_in = 300
          token_type = 'Bearer'
          scope = None
          refresh_token = token.refresh_token

          return {
               'access_token': access_token,
               'expires_in': expires_in,
               'token_type': token_type,
               'scope': scope,
               'refresh_token': refresh_token
          }