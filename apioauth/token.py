from email.utils import localtime
from .models import TokenAuth, User
import pytz
from datetime import datetime, timedelta
import hashlib

timezone = pytz.timezone('Asia/Jakarta')
def get_access_token(username, client_id):
     jakarta_time = datetime.now(timezone)
     print(jakarta_time)
     message = "access" + username + client_id + jakarta_time.strftime("%m%d%Y%H%M%S")
     encode_message = hashlib.sha1(message.encode("utf-8"))
     token = encode_message.hexdigest()
     return token

def get_refresh_token(username, client_id):
     jakarta_time = datetime.now(timezone)
     message = "refresh" + username + client_id + jakarta_time.strftime("%m%d%Y%H%M%S")
     encode_message = hashlib.sha1(message.encode("utf-8"))
     token = encode_message.hexdigest()
     return token

def generate_token(username, client_id):
     access_token = get_access_token(username, client_id)
     refresh_token = get_refresh_token(username, client_id)
     user_instance = User.objects.get(username=username, client_id=client_id)
     
     token_object = TokenAuth.objects.create(
          user = user_instance,
          access_token = access_token,
          refresh_token = refresh_token
     )
     return token_object

def verify_token(token):
     try:
          token_type, token_content = token.split()
          if token_type != 'Bearer':
               return False
          
          if not check_token_exists_and_not_expired(token_content):
               return False
               
     except ValueError: #Kalau token gabisa di split
          return False
     return True

def check_token_exists_and_not_expired(token):
     token_object_by_access_token = TokenAuth.objects.filter(access_token=token)
     if not len(token_object_by_access_token):
          return False
     token_obj = token_object_by_access_token[0]
     created_time_utc = token_obj.created_at #dalam waktu utc
     
     created_time = created_time_utc + timedelta(hours=7) #biar jadi waktu jakarta
     current_time = datetime.now(timezone)

     '''
          https://www.geeksforgeeks.org/how-to-convert-datetime-to-integer-in-python/
          Berarti kalau 500 = 5 menit
     '''
     diff_current_created = int(current_time.strftime("%Y%m%d%H%M%S")) - int(created_time.strftime("%Y%m%d%H%M%S"))
     if diff_current_created > 500:
          return False           
     return True