from apioauth.models import TokenAuth
from apioauth.token import verify_token
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import ObtainTokenSerializer
from .models import UserProfile
class ObtainTokenView(APIView):
     serializer_class = ObtainTokenSerializer

     def post(self, request):
          serializer = self.serializer_class(data=request.data)
          if serializer.is_valid():
               #Blok ini menandakan serializer nya valid
               response = serializer.validated_data
               return Response(
                    response,
                    status=status.HTTP_200_OK
               )
          return Response(
               {
                    "error": "invalid_request",
                    "Error_description": "ada kesalahan masbro!"
               },
               status=status.HTTP_401_UNAUTHORIZED
          )

class ObtainProfileview(APIView):
     
     def post(self, request):
          token = request.META.get('HTTP_AUTHORIZATION')
          if verify_token(token):
               split_token = token.split()
               token_objects_by_access_token = TokenAuth.objects.filter(access_token=split_token[1])
               token_obj = token_objects_by_access_token[0]
               user = token_obj.user
               profile_objects_by_user = UserProfile.objects.filter(user = user)
               profile_obj = profile_objects_by_user[0]
               return Response(
                    {
                         "access_token": token_obj.access_token,
                         "client_id": user.client_id,
                         "user_id": user.username,
                         "full_name": profile_obj.full_name,
                         "npm": profile_obj.npm,
                         "expires": None,
                         "refresh_token": token_obj.refresh_token
                    },
                    status=status.HTTP_200_OK
               )
          return Response(
               {
                    "error": "invalid_token",
                    "error_description": "Token Salah masbro"
               },
               status=status.HTTP_401_UNAUTHORIZED
          )