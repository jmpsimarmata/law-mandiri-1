from django.urls import path
from .views import ObtainTokenView, ObtainProfileview

app_name = 'apioauth'
urlpatterns = [
     path('token', ObtainTokenView.as_view(), name='obtainTokenView'),
     path('resource', ObtainProfileview.as_view(), name='obtainProfileView')
]