from django.apps import AppConfig


class ApioauthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apioauth'
